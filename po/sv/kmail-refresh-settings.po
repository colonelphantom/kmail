# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kmail package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kmail\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-02 01:04+0000\n"
"PO-Revision-Date: 2022-01-02 14:20+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Stefan Asserhäll"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "stefan.asserhall@bredband.net"

#: main.cpp:32 main.cpp:34
#, kde-format
msgid "KMail Assistant for refreshing settings"
msgstr "Kmail-assistent för att uppdatera inställningar"

#: main.cpp:36
#, fuzzy, kde-format
#| msgid "(c) 2019-2022 Laurent Montel <montel@kde.org>"
msgid "(c) 2019-2023 Laurent Montel <montel@kde.org>"
msgstr "© 2019-2022 Laurent Montel <montel@kde.org>"

#: main.cpp:37
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: main.cpp:37
#, kde-format
msgid "Author"
msgstr "Upphovsman"

#: refreshsettingsassistant.cpp:22
#, kde-format
msgctxt "@title:window"
msgid "KMail Refresh Settings"
msgstr "Kmail uppdatera inställningar"

#: refreshsettingsassistant.cpp:39
#, kde-format
msgid "Warning"
msgstr "Varning"

#: refreshsettingsassistant.cpp:43
#, kde-format
msgid "Clean up Settings"
msgstr "Rensa inställningar"

#: refreshsettingsassistant.cpp:47
#, kde-format
msgid "Finish"
msgstr "Slutför"

#: refreshsettingscleanuppage.cpp:20
#, kde-format
msgid "Clean"
msgstr "Rensa"

#: refreshsettingscleanuppage.cpp:50
#, kde-format
msgid "Remove obsolete \"TipOfDay\" settings: Done"
msgstr "Ta bort föråldrade inställningar för \"Dagens tips\": Klar"

#: refreshsettingscleanuppage.cpp:62
#, kde-format
msgid "Delete Dialog settings in file `%1`: Done"
msgstr "Ta bort dialoginställningar i filen `%1`: Klar"

#: refreshsettingscleanuppage.cpp:74
#, kde-format
msgid "Delete Filters settings in file `%1`: Done"
msgstr "Ta bort filterinställningar i filen `%1`: Klar"

#: refreshsettingscleanuppage.cpp:90
#, kde-format
msgid "Clean Folder Settings in setting file `%1`: Done"
msgstr "Rensa kataloginställningar i inställningsfilen `%1`: Klar"

#: refreshsettingscleanuppage.cpp:146
#, kde-format
msgid "Clean Dialog Size in setting file `%1`: Done"
msgstr "Rensa dialogstorlek i inställningsfilen `%1`: Klar"

#: refreshsettingsfirstpage.cpp:18
#, kde-format
msgid "Please close KMail/Kontact before using it."
msgstr "Stäng Kmail och Kontact innan den används."

#~ msgid "PIM Import Tool"
#~ msgstr "Importverktyg för personlig information"
